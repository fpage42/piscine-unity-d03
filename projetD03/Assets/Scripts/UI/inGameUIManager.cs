﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inGameUIManager : MonoBehaviour {

    public Texture2D cursorImage;

    void Start () {
        Cursor.SetCursor(this.cursorImage, Vector2.zero, CursorMode.Auto);
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameManager.gm.pause(true);
        }
	}

    public void setCursor(Texture2D texture)
    {
        if (texture == null)
            Cursor.SetCursor(this.cursorImage, Vector2.zero, CursorMode.Auto);
        else
            Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);
    }
}
