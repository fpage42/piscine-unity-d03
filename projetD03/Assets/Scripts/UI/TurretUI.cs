﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TurretUI : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject itemBeingDragged;
    public inGameUIManager managerUI;
    public towerScript tower;
    private bool isUse = false;


    public void OnBeginDrag(PointerEventData eventData)
    {
        if (gameManager.gm.playerEnergy >= this.tower.energy)
        {
            managerUI.setCursor(GetComponent<SpriteRenderer>().sprite.texture);
            this.isUse = true;
        }
        }

    public void OnDrag(PointerEventData eventData)
    {
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        managerUI.setCursor(null);
        this.isUse = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameManager.gm.playerEnergy < this.tower.energy)
            GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
        else
        {
            if (Input.GetMouseButtonUp(0) && isUse == true)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector3.zero);
                if (hit)
                {
                    if (hit.collider.gameObject.tag != "tower")
                    {
                        Vector3 spawnPos = new Vector3(hit.transform.position.x, hit.transform.position.y, -1);
                        GameObject.Instantiate(this.tower, spawnPos, Quaternion.identity);
                        gameManager.gm.playerEnergy -= this.tower.energy;
                        this.isUse = false;
                    }
                }
            }
        }
    }
}
